#!/bin/bash

gitlab_local=/opt/gitlab
http_port=4080
https_port=4434
ssh_port=4222

docker stop gitlab
docker rm gitlab
docker run -d \
           --publish ${http_port}:80 \
           --publish ${https_port}:443 \
           --publish ${ssh_port}:22 \
           --name gitlab \
           --hostname gitlab \
           -v ${gitlab_local}/config:/etc/gitlab \
           -v ${gitlab_local}/logs:/var/log/gitlab \
           -v ${gitlab_local}/data:/var/opt/gitlab \
           gitlab/gitlab-ce:latest
