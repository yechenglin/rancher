#!/bin/sh

# install helm
helm_version=2.16.1
helm_tgz=helm-v${helm_version}-linux-amd64.tar.gz
dest_dir=/usr/local/bin

wget https://get.helm.sh/${helm_tgz} \
  && tar xzfp ${helm_tgz} -C ${dest_dir} --strip-components=1 linux-amd64/helm \
  && rm ${helm_tgz}

# deploy tiller
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF

helm init --service-account tiller
