#!/bin/bash

jenkins_local=/opt/jenkins
http_port=5080

mkdir -p ${jenkins_local}
chown 1000 ${jenkins_local}

docker stop jenkins
docker rm jenkins
docker run -d \
           -v ${jenkins_local}:/var/jenkins_home \
           -p ${http_port}:8080 \
           -p 50000:50000 \
           --name jenkins \
           --hostname jenkins \
           jenkins/jenkins:lts
