#!/bin/bash

harbor_version=1.9.3
harbor_local=/opt/harbor
harbor_tgz=harbor-offline-installer-v${harbor_version}.tgz

enable_notary="--with-notary"
enable_clair="--with-clair"
enable_chart="--with-chartmuseum"

cert_path=/etc/docker/certs.d/${HOST_IP}

mkdir -p ${cert_path}

wget https://github.com/goharbor/harbor/releases/download/v${harbor_version}/${harbor_tgz} \
  tar xvf ${harbor_tgz}

# install docker-compose v1.24.1
wget "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -P /usr/local/bin/
mv /usr/local/bin/docker-compose-$(uname -s)-$(uname -m) /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# generate ssl key
openssl req -newkey rsa:4096 -nodes -sha256 -keyout ${cert_path}/ca.key -x509 -days 3650 -out ${cert_path}/ca.crt
