# K8s-PRJ

---

A project based on **Kubernetes**.

## Rancher

---

1. Install rancher

```
$ sudo ./init-rancher.sh
```

2. Login rancher

> https://www.rancher.cn/docs/rancher/v2.x/cn/overview/quick-start-guide/#%E5%9B%9B-%E7%99%BB%E5%BD%95rancher



## K8s

---

### Create k8s cluster

> https://www.rancher.cn/docs/rancher/v2.x/cn/overview/quick-start-guide/#%E4%BA%94-%E5%88%9B%E5%BB%BAk8s%E9%9B%86%E7%BE%A4

### Configure kubeconfig

1. find kubeconfig
![k8s-node-dashboard](./images/k8s-node-dashboard.png)

2. copy kubeconfig to the host where running rancher

    ```
    $ ls ~/.kube/config
    ```

3. check k8s CLI result

    ```
    $ kubectl get node
    ```

## Clean node

当节点无法访问且无法使用自动清理，或者异常导致节点脱离集群后，如果需要再次将节点加入集群，那么需要手动进行节点初始化操作。

    ```
    $ sudo ./clean-node.sh
    ```


## Helm

---

```
$ ./scripts/init-helm.sh
```

## CI/CD

---

### Gitlab

### Jinkens

### Harbor

1. export `HOST_IP`

    ```
    $ export HOST_IP=192.168.0.52
    ```

    **NOTE:** replace `192.168.0.52` with your host ip.

2. install harbor

    ```
    $ ./run-harbor.sh
    <...>
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:
    State or Province Name (full name) [Some-State]:
    Locality Name (eg, city) []:
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (e.g. server FQDN or YOUR name) []:192.168.0.52    <-- should with your host ip.
    Email Address []:
    <...>
    ```
