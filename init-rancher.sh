#!/bin/sh

RANCHER_LOCAL=/opt/rancher
HTTP_PORT_LOCAL=3080
HTTPS_PORT_LOCAL=3443

# init rancher
docker run -d \
           --restart=unless-stopped \
           -v $RANCHER_LOCAL:/var/lib/rancher/ \
           -p $HTTP_PORT_LOCAL:80 \
           -p $HTTPS_PORT_LOCAL:443 \
           rancher/rancher:stable


# install k8s CLI
wget -qO - https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
  sudo apt-key add
sudo touch /etc/apt/sources.list.d/kubernetes.list
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | \
  sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
